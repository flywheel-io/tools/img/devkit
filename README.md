# flywheel/devkit

Utility image for managing Flywheel applications from within a cluster.

## Usage

```bash
# in local dev environments
docker run --rm -itv=$HOME/.kube:/root/.kube flywheel/devkit
# in staging/prod envs (as a drop-in replacement for infrastructure-tools)
docker run --rm -it flywheel/devkit gke|eks <region> <cluster-name>
```
