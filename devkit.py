#!/usr/bin/env python
"""Backend clients ready to use.

HTTP:    fw.get("/api/config")
Mongo:   db.users.find_one({})
Redis:   redis.keys()
Elastic: es.info()
"""
from pprint import pprint as pp  # pylint: disable=unused-import

from dotty_dict import Dotty
from elasticsearch import Elasticsearch
from fw_client import FWClient
from pydantic import BaseSettings
from pymongo import MongoClient
from redis import Redis


class Config(BaseSettings):
    """Devkit config - use envvars to set/override."""

    FW_API_KEY = ""
    FW_DRONE_SECRET = ""
    CORE_URL = "http://flywheel-core-api:8080"
    XFER_URL = "http://flywheel-xfer:8000"
    IO_PROXY_URL = "http://flywheel-io-proxy:8000"
    SNAPSHOT_URL = "http://flywheel-snapshot:8000"
    MONGO_URL = "mongodb://flywheel-mongodb-replicaset-client:27017/scitran?replicaSet=elasticflywheel"
    REDIS_URL = "redis://flywheel-redis-master:6379/0"
    RABBIT_URL = "amqp://fw:change-me@flywheel-rabbitmq:5672/%2F"
    ELASTIC_URL = "http://elasticsearch-master:9200"


class dotdict(Dotty, dict):
    """Dictionary wrapper with dot-notated-key *and* attribute access."""

    __slots__ = "_data", "separator", "esc_char", "no_list"

    def __init__(self, *args, **kwargs):
        """Init dotdict from a plain-old dict."""
        data = args[0] if args and isinstance(args[0], dict) else dict(*args, **kwargs)
        super().__init__(data)

    def __repr__(self) -> str:
        """Return string representation."""
        return repr(self._data)

    def __getitem__(self, key):
        """Return value for key (wrapping sub-dicts)."""
        value = super().__getitem__(key)
        return type(self)(value) if isinstance(value, dict) else value

    def __getattr__(self, name: str):
        """Return value for attribute name handled as a dict key."""
        keys = [name]
        if name == "id":
            keys.append("_id")
        elif "_" in name:
            keys.append(name.replace("_", "-"))
        for key in keys:
            try:
                return self[key]
            except KeyError:
                pass
        msg = f"{type(self).__name__!r} object has no attribute {name!r}"
        raise AttributeError(msg) from None


config = Config()

fw = FWClient(
    api_key=config.FW_API_KEY,
    drone_secret=config.FW_DRONE_SECRET,
    device_label="devkit",
    defer_auth=not config.FW_API_KEY and not config.FW_DRONE_SECRET,
    url=config.CORE_URL,
    xfer_url=config.XFER_URL,
    io_proxy_url=config.IO_PROXY_URL,
    snapshot_url=config.SNAPSHOT_URL,
    client_name="devkit",
    client_version="0.1.0",
    connect_timeout=5,
    read_timeout=10,
    retry_total=0,
)

# TODO fix client flakyness wrt "missing clusterTime"
mongo = MongoClient(
    config.MONGO_URL,
    appname="devkit",
    journal=True,
    tz_aware=True,
    document_class=dotdict,
    connectTimeoutMS=5000,
    serverSelectionTimeoutMS=5000,
)

db = mongo.get_default_database()

redis = Redis.from_url(
    config.REDIS_URL,
    health_check_interval=10,
    socket_keepalive=True,
)

es = Elasticsearch(
    config.ELASTIC_URL,
    request_timeout=5,
)
