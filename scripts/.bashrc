#!/bin/bash
# shellcheck disable=SC1091
case $- in
    *i*) ;;
    *) return;;
esac

# shellcheck disable=SC2046
unset $(env | sed -En 's/(^(PYTHON|V)_[^=]+)=.*/\1/p')
export BAT_THEME=base16
export FZF_DEFAULT_OPTS="--height=~8 --layout=reverse"
export HISTCONTROL=ignoreboth:erasedups
. /etc/bash_completion
. /etc/fzf_key_bindings

alias ps='ps aux'
alias less='less -FSRXc'
alias grep='grep -E --color=auto'
alias sed='sed -E'
alias cat='bat'
alias ls='exa -ahl --git --group-directories-first --time-style=long-iso'
alias tree='tree --dirsfirst --sort=version'
alias help='tldr'; complete -F _tealdeer help
alias ktl=kubectl; complete -F __start_kubectl ktl
alias kc=kubectl;  complete -F __start_kubectl kc
alias k=kubectl;   complete -F __start_kubectl k
alias f=fwctl; complete -F _fwctl f
k9s() { command k9s --namespace="${NAMESPACE:-default}" "$@"; }

PROMPT_COMMAND=prompt_command
prompt_command() {
    # save the last exit code, append cmd to history
    local STATUS=$?
    history -a
    # define colors and start from empty ps1 for incremental build
    ansi() { printf "\[\033[%sm\]" "$1"; }
    local MOD=(NORM BOLD DIM)
    local CLR=(BLACK RED GREEN YELLOW BLUE MAGENTA CYAN WHITE)
    for M in "${!MOD[@]}"; do local "${MOD[$M]}=$(ansi "$M")"; done
    for C in "${!CLR[@]}"; do local "${CLR[$C]}=$(ansi "0;3$C")"; done
    export PS1=
    # cloud provider and region envvars - from eks/gke entrypoint args
    if [[ -n "$CLOUD" ]]; then
        PS1+="${DIM}(cloud:${BLUE}${BOLD}${CLOUD}${NORM}"
        PS1+="${DIM}/${BLUE}${REGION:-none}${NORM}"
        PS1+="${DIM})${NORM} "
    fi
    # cluster name and namespace - from eks/gke entrypoint or scraped
    local CLUSTER=${CLUSTER_NAME:-"$(kubectl config current-context 2>/dev/null)"}
    if [[ -n "$CLUSTER" ]]; then
        PS1+="${DIM}(kube:${BLUE}${BOLD}${CLUSTER}${NORM}"
        PS1+="${DIM}/${BLUE}${NAMESPACE:-default}${NORM}"
        PS1+="${DIM})${NORM} "
    fi
    # user@host - try to get site domain, fall back to container hostname
    [[ -z "$PS1" ]] || PS1+="\n"
    local HOST
    [[ -f /tmp/ps1.host ]] || kubectl get ingress -ojson | jq -r '.items[0].spec.rules[0].host' >/tmp/ps1.host 2>/dev/null
    [[ -s /tmp/ps1.host ]] && HOST=$(cat /tmp/ps1.host) || HOST='\h'
    PS1+="${DIM}\\u@${HOST}${NORM} "
    # add current working directory
    PS1+="${CYAN}${BOLD}\\W${NORM} "
    # add final prompt character
    [[ "$STATUS" = 0 ]] && PS1+="${GREEN}${BOLD}" || PS1+="${RED}${BOLD}"
    PS1+="❯${NORM} "
}

[[ -f /tmp/banner ]] || { bat -l=sh --style=plain; touch /tmp/banner; } <<'EOF'
# Run `help cmd` for help, hit <TAB> to auto-complete, ^R for history, ^D to exit
fwctl status         # show pod statuses for cluster overview
fwctl logs core-api  # display formatted and colorized log tail
fwctl --help         # see the help for more commands and options
EOF
