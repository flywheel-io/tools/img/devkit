FROM python:3.11-slim
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
RUN apt-get update; \
    apt-get install -y --no-install-recommends \
        bash \
        bash-completion \
        bsdmainutils \
        curl \
        dnsutils \
        git \
        groff \
        iputils-ping \
        jo \
        jq \
        less \
        moreutils \
        net-tools \
        netcat-traditional \
        openssh-client \
        procps \
        telnet \
        traceroute \
        tree \
        unzip \
        vim-tiny \
        xz-utils \
        zip \
    ; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

WORKDIR /opt
RUN curl -fLSso awscli2.zip https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip; \
    unzip awscli2.zip; \
    ./aws/install; \
    rm -rf awscli2.zip
ENV V_GCLOUD_SDK=410.0.0
ENV PATH=$PATH:/opt/google-cloud-sdk/bin
RUN curl -fLSs https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${V_GCLOUD_SDK}-linux-x86_64.tar.gz | tar xz; \
    ./google-cloud-sdk/install.sh --override-components gke-gcloud-auth-plugin; \
    gcloud config set component_manager/disable_update_check true

WORKDIR /usr/local/bin
ENV V_AWS_IAM_AUTHENTICATOR=0.5.9
RUN curl -fLSso aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v${V_AWS_IAM_AUTHENTICATOR}/aws-iam-authenticator_${V_AWS_IAM_AUTHENTICATOR}_linux_amd64; \
    chmod +x aws-iam-authenticator
ENV V_BAT=v0.22.1
RUN curl -fLSs https://github.com/sharkdp/bat/releases/download/${V_BAT}/bat-${V_BAT}-x86_64-unknown-linux-gnu.tar.gz \
        | tar xz --strip-components=1 bat-${V_BAT}-x86_64-unknown-linux-gnu/bat
ENV V_EXA=v0.10.1
RUN curl -fLSso exa.zip https://github.com/ogham/exa/releases/download/${V_EXA}/exa-linux-x86_64-${V_EXA}.zip; \
    unzip exa.zip bin/exa; \
    mv bin/exa .; \
    rm -rf bin exa.zip
ENV V_FD=v8.5.3
RUN curl -fLSs https://github.com/sharkdp/fd/releases/download/${V_FD}/fd-${V_FD}-x86_64-unknown-linux-gnu.tar.gz \
        | tar xz --strip-components=1 fd-${V_FD}-x86_64-unknown-linux-gnu/fd
ENV V_FZF=0.35.1
RUN curl -fLSs https://github.com/junegunn/fzf/releases/download/${V_FZF}/fzf-${V_FZF}-linux_amd64.tar.gz \
        | tar xz; \
    curl -fLSso /etc/bash_completion.d/fzf https://raw.githubusercontent.com/junegunn/fzf/${V_FZF}/shell/completion.bash; \
    curl -fLSso /etc/fzf_key_bindings https://raw.githubusercontent.com/junegunn/fzf/${V_FZF}/shell/key-bindings.bash
ENV V_MICRO=2.0.11
ENV EDITOR=micro
RUN curl -fLSs https://github.com/zyedidia/micro/releases/download/v${V_MICRO}/micro-${V_MICRO}-linux64.tar.gz \
		| tar xz --strip-components=1 micro-${V_MICRO}/micro
ENV V_MONGOSH=1.6.0
RUN curl -fLSs https://downloads.mongodb.com/compass/mongosh-${V_MONGOSH}-linux-x64.tgz \
		| tar xz --strip-components=2 mongosh-${V_MONGOSH}-linux-x64/bin
ENV V_MONGOTOOLS=100.6.1
RUN curl -fLSs https://fastdl.mongodb.org/tools/db/mongodb-database-tools-debian10-x86_64-${V_MONGOTOOLS}.tgz \
		| tar xz --strip-components=2 mongodb-database-tools-debian10-x86_64-${V_MONGOTOOLS}/bin
ENV V_K9S=v0.26.7
RUN curl -fLSs https://github.com/derailed/k9s/releases/download/${V_K9S}/k9s_Linux_x86_64.tar.gz \
		| tar xz k9s
ENV V_KUBECTL=v1.23.14
RUN curl -fLSsO https://storage.googleapis.com/kubernetes-release/release/${V_KUBECTL}/bin/linux/amd64/kubectl; \
	chmod +x kubectl; \
    kubectl completion bash >/etc/bash_completion.d/kubectl
ENV V_RG=13.0.0
RUN curl -fLSs https://github.com/BurntSushi/ripgrep/releases/download/${V_RG}/ripgrep-${V_RG}-x86_64-unknown-linux-musl.tar.gz \
        | tar xz --strip-components=1 ripgrep-${V_RG}-x86_64-unknown-linux-musl/rg
ENV V_STERN=1.22.0
RUN curl -fLSs stern https://github.com/stern/stern/releases/download/v${V_STERN}/stern_${V_STERN}_linux_amd64.tar.gz \
        | tar xz stern; \
    stern --completion bash >/etc/bash_completion.d/stern
ENV V_TEALDEER=v1.6.1
RUN curl -fLSso tldr https://github.com/dbrgn/tealdeer/releases/download/${V_TEALDEER}/tealdeer-linux-x86_64-musl; \
    curl -fLSso /etc/bash_completion.d/kubectl https://github.com/dbrgn/tealdeer/releases/download/${V_TEALDEER}/completions_bash; \
    chmod +x tldr; \
    tldr --seed-config; sed -Ei "s/(compact = )false/\1true/" /root/.config/tealdeer/config.toml; \
    tldr --update; \
    rm -rf /root/.cache/tealdeer/tldr-pages/pages.*; \
    rm -rf /root/.cache/tealdeer/tldr-pages/pages/{android,osx,sunos,windows}
ENV V_YQ=v4.30.5
RUN curl -fLSso yq https://github.com/mikefarah/yq/releases/download/${V_YQ}/yq_linux_amd64; \
    chmod +x yq

WORKDIR /root
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY pyproject.toml devkit.py ./
RUN pip install --no-cache-dir --no-deps --editable .
ENV PATH=/root/scripts:$PATH
COPY . .
RUN SITE=$(python -c 'import site; print(site.getsitepackages()[0])'); \
    sed -i 's/bg:ansiyellow/bg:#882222/' "$SITE/IPython/core/ultratb.py"; \
    fwctl --comp >/etc/bash_completion.d/fwctl; \
    ln -sf /root/scripts/.bashrc ./

ENTRYPOINT []
CMD ["/bin/bash"]
